$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval:500
    });
    $('#contactoModal').on('show.bs.modal',function(e){
        console.log('El mensaje se está mostrando...');
        $('#contactoBtn').removeClass('btn-warning');
        $('#contactoBtn').addClass('btn-success');
        $('#contactoBtn').prop('disabled', true);
    });
    $('#contactoModal').on('shown.bs.modal',function(e){
        console.log('El mensaje se mostró...');
    });
    $('#contactoModal').on('hide.bs.modal',function(e){
        console.log('El mensaje se está ocultando...');
    });
    $('#contactoModal').on('hidden.bs.modal',function(e){
        console.log('El mensaje se ocultó...');
        $('#contactoBtn').prop('disabled', false);
    });
});